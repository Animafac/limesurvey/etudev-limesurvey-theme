# etudev-limesurvey-theme

E&D LimeSurvey theme (based on vanilla) used on [forms.animafac.net](https://forms.animafac.net/)

## Install

This theme needs to be install in the `upload/themes/survey/` folder of your LimeSurvey instance and then enabled in the LimeSurvey admin.

## Web app manifest

The `manifest.json` allows LimeSurvey to be used as a [progressive web app](https://developers.google.com/web/progressive-web-apps/).

## Scripts

* `disable-answsers.js` disable answer to multiple choice question when they end with `(complet)`.
