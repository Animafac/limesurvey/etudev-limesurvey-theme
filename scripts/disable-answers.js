/*jslint browser: true*/
/*global $*/
$(window).load(
    function () {
        'use strict';
        $('.radio-label + .label-text').each(
            function () {
                if ($(this).text().trim().endsWith('(complet)')) {
                    $(this).parent().find('input').prop('disabled', true);
                }
            }
        );
    }
);
